﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AccountService.Data;
using AccountService.Models;
using RabbitMQEventbus.Extension.Eventbus;
using AccountService.Models.DTO;
using RabbitMQEventbus.Extension.Message;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Net.Http.Headers;
using System.Security.Claims;

namespace AccountService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountsController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly IRabbitMQEventbus _eventbus;

        public AccountsController(ApplicationDbContext context, IRabbitMQEventbus eventbus)
        {
            _context = context;
            _eventbus = eventbus;
        }

        [HttpGet]
        public async Task<ActionResult<Account>> GetAccount()
        {
            var bearerToken = Request.Headers[HeaderNames.Authorization].ToString();

            if (String.IsNullOrWhiteSpace(bearerToken))
            {
                return Unauthorized();
            }

            var jwt = bearerToken.Replace("Bearer ", "");

            var handler = new JwtSecurityTokenHandler();
            var token = handler.ReadJwtToken(jwt);



            var userId = token.Claims.FirstOrDefault(x => x.Type == "sub").Value.ToString();

            var account = await _context.Accounts.FirstOrDefaultAsync(x => x.UserId == Guid.Parse(userId));

            if (account == null)
            {
                return NotFound();
            }
                
            return account;
        }

        // PUT: api/Accounts/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAccount(Guid id, Account account)
        {
            if (id != account.UserId)
            {
                return BadRequest();
            }

            _context.Entry(account).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AccountExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Accounts
        [HttpPost]
        public async Task<ActionResult<Account>> PostAccount(Account account)
        {
            _context.Accounts.Add(account);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetAccount", new { id = account.UserId }, account);
        }

        // DELETE: api/Accounts/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAccount(Guid id)
        {
            var account = await _context.Accounts.FindAsync(id);
            if (account == null)
            {
                return NotFound();
            }

            _context.Accounts.Remove(account);
            await _context.SaveChangesAsync();

            var message = new DeleteAccountMessage()
            {
                UserId = account.UserId,
            };

            var json = Newtonsoft.Json.JsonConvert.SerializeObject(message);
            _eventbus.Publish(new RabbitMQMessage(new MessageDestination("AccountsExchange", "accounts.delete"), json));

            return NoContent();
        }

        private bool AccountExists(Guid id)
        {
            return _context.Accounts.Any(e => e.UserId == id);
        }
    }
}
