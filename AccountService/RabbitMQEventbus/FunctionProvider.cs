﻿using System;
using System.Collections.Generic;
using System.Text;
using AccountService.Data;
using AccountService.Models;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client.Events;
using RabbitMQEventbus.Extension.FunctionProvider;
using RabbitMQEventbus.Extension.Message;

namespace AccountService.Api.RabbitMQEventbus
{
    public class FunctionProvider : RabbitMQFunctionProviderBase
    {
        private readonly ILogger<FunctionProvider> _logger;
        private readonly IServiceProvider _serviceProvider;

        public FunctionProvider(ILogger<FunctionProvider> logger, IServiceProvider serviceProvider)
        {
            _logger = logger;
            _serviceProvider = serviceProvider;
        }

        protected override Dictionary<string, Func<object, BasicDeliverEventArgs, RabbitMQMessage>> InializeFunctionsWithKeys()
        {
            var dictionary = new Dictionary<string, Func<object, BasicDeliverEventArgs, RabbitMQMessage>>();
            dictionary.Add(nameof(HandleNewAccount), HandleNewAccount);

            return dictionary;
        }

        private RabbitMQMessage HandleNewAccount(object o, BasicDeliverEventArgs eventArgs)
        {
            var body = eventArgs.Body.ToArray();
            var message = Encoding.UTF8.GetString(body);

            try
            {
                var account = Newtonsoft.Json.JsonConvert.DeserializeObject<Account>(message);

                using (var scope = _serviceProvider.CreateScope())
                {
                    var context = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();

                    account.CreatedOn = DateTime.Now;
                    context.Accounts.Add(account);

                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                _logger.LogWarning(e.StackTrace);
            }

            return null;
        }
    }
}
