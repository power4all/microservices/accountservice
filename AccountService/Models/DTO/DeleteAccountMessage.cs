﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AccountService.Models.DTO
{
    public class DeleteAccountMessage
    {
        public Guid UserId { get; set; }
    }
}
